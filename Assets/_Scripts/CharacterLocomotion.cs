﻿using UnityEngine;

public class CharacterLocomotion : MonoBehaviour
{
    Animator animator;
    Vector2 input;
    int inputXHash;
    int inputYHash;

    private void Awake()
    {
        animator = GetComponent<Animator>();
        inputXHash = Animator.StringToHash("InputX");
        inputYHash = Animator.StringToHash("InputY");
    }

    void Update()
    {
        input.x = Input.GetAxis("Horizontal");
        input.y = Input.GetAxis("Vertical");

        animator.SetFloat(inputXHash, input.x);
        animator.SetFloat(inputYHash, input.y);
    }
}
