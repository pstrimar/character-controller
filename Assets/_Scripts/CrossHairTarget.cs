﻿using UnityEngine;

public class CrossHairTarget : MonoBehaviour
{
    Camera mainCam;
    Ray ray;

    void Awake()
    {
        mainCam = Camera.main;
    }

    // Update is called once per frame
    void Update()
    {
        ray.origin = mainCam.transform.position;
        ray.direction = mainCam.transform.forward;
        Physics.Raycast(ray, out RaycastHit hitInfo);
        transform.position = hitInfo.point;
    }
}
